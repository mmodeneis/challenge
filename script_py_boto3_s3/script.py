#!/usr/bin/env python3

import boto3
import subprocess
import tempfile

def get_ebs_size(volume_id):
    ec2 = boto3.client('ec2', region_name='eu-central-1')
    response = ec2.describe_volumes(VolumeIds=[volume_id])
    volume_size = response['Volumes'][0]['Size']
    return volume_size

def get_instance_ebs_size(instance):
    ebs_volumes = instance.get('BlockDeviceMappings', [])
    ebs_size = sum([get_ebs_size(volume['Ebs']['VolumeId']) for volume in ebs_volumes if 'Ebs' in volume])
    return ebs_size

def get_ebs_count(instance):
    ebs_volumes = instance.get('BlockDeviceMappings', [])
    ebs_count = len([volume for volume in ebs_volumes if 'Ebs' in volume])
    return ebs_count

def print_server_details(instances, output_file):
    output_file.write(f"{'Instance ID':<20} {'Instance Type':<12} {'Status':<10} {'Private IP':<15} {'Public IP':<15} {'Total EBS Size (GB)':<20} {'EBS Count':<10} {'EBS Volume IDs':<40}\n")
    output_file.write('-' * 145 + '\n')
    total_ebs_size = 0
    for instance in instances:
        instance_id = instance['InstanceId']
        instance_type = instance['InstanceType']
        status = instance['State']['Name']
        private_ip = instance.get('PrivateIpAddress', 'N/A')
        public_ip = instance.get('PublicIpAddress', 'N/A')
        ebs_size = get_instance_ebs_size(instance)
        ebs_count = get_ebs_count(instance)
        ebs_volumes = instance.get('BlockDeviceMappings', [])
        ebs_volume_ids = ', '.join([volume['Ebs']['VolumeId'] for volume in ebs_volumes if 'Ebs' in volume])
        total_ebs_size += ebs_size

        output_file.write(f"{instance_id:<20} {instance_type:<12} {status:<10} {private_ip:<15} {public_ip:<15} {ebs_size:>20} GB {ebs_count:<10} {ebs_volume_ids:<40}\n")

    output_file.write(f"\nTotal EBS Size of all servers: {total_ebs_size} GB\n")

def main():
    ec2 = boto3.client('ec2', region_name='eu-central-1')
    
    search_input = input("Digite o nome da Instancia ou use '*' para listar todos os servidores: ")
    
    filters = []
    if search_input != '*':
        filters.append({
            'Name': 'tag:Name',
            'Values': [search_input]
        })
    
    instances = ec2.describe_instances(Filters=filters)['Reservations']
    instances = [instance['Instances'][0] for instance in instances]
    
    instances.sort(key=lambda x: get_instance_ebs_size(x), reverse=True)
    
    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        tmp_file_path = tmp_file.name
        with open(tmp_file_path, 'w') as f:
            print_server_details(instances, f)
    
    subprocess.run(["aws", "s3", "cp", tmp_file_path, "s3://challenge-results/"])
    
if __name__ == "__main__":
    main()
