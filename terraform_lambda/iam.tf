resource "aws_iam_role" "lambda_execution_role" {
  name = "lambda_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Action = "sts:AssumeRole",
      Effect = "Allow",
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_iam_policy" "lambda_ec2_describe_volumes_policy" {
  name        = "LambdaEC2DescribeVolumesPolicy"
  description = "Policy for Lambda to describe EC2 volumes"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Action = "ec2:DescribeVolumes",
      Effect   = "Allow",
      Resource = "*"
    }]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_ec2_describe_volumes_policy_attachment" {
  policy_arn = aws_iam_policy.lambda_ec2_describe_volumes_policy.arn
  role       = aws_iam_role.lambda_execution_role.name
}


resource "aws_iam_policy" "lambda_basic_execution_policy" {
  name        = "LambdaBasicExecutionPolicy"
  description = "Policy for Lambda Basic Execution"
  
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Effect   = "Allow",
        Resource = "*"
      },
      {
        Action = [
          "ec2:DescribeInstances"  
        ],
        Effect   = "Allow",
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_execution_policy_attachment" {
  policy_arn = aws_iam_policy.lambda_basic_execution_policy.arn
  role       = aws_iam_role.lambda_execution_role.name
}

resource "aws_iam_policy" "lambda_s3_write_policy" {
  name        = "LambdaS3WritePolicy"
  description = "Policy for Lambda to write to S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Action = [
        "s3:PutObject",
        "s3:ListBucket"
      ],
      Effect   = "Allow",
      Resource = [
        "arn:aws:s3:::challenge-results",
        "arn:aws:s3:::challenge-results/*"
      ]
    }]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_s3_write_policy_attachment" {
  policy_arn = aws_iam_policy.lambda_s3_write_policy.arn
  role       = aws_iam_role.lambda_execution_role.name
}
