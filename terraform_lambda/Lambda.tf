resource "aws_lambda_function" "lambda_function" {
  filename      = "lambda_function.zip"
  function_name = "EC2ServerInfo"
  role          = aws_iam_role.lambda_execution_role.arn
  handler       = "script.lambda_handler"
  source_code_hash = filebase64sha256("lambda_function.zip")
  runtime       = "python3.8"
  timeout       = 600
  
  tracing_config {
    mode = "Active"
  }
}

data "archive_file" "lambda_function" {
  type        = "zip"
  source_dir = "lambda_source"
  output_path = "lambda_function.zip"
}
