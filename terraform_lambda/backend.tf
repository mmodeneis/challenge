terraform {
  backend "s3" {
    bucket         = "challenge-results"
    key            = "terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "challenge-cocus"
  }
}