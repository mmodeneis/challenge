import logging
import boto3
import tempfile
import os
import sys

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_ebs_size(volume_id):
    ec2 = boto3.client('ec2', region_name='eu-central-1')
    response = ec2.describe_volumes(VolumeIds=[volume_id])
    volume_size = response['Volumes'][0]['Size']
    return volume_size

def get_instance_ebs_size(instance):
    ebs_volumes = instance.get('BlockDeviceMappings', [])
    ebs_size = sum([get_ebs_size(volume['Ebs']['VolumeId']) for volume in ebs_volumes if 'Ebs' in volume])
    return ebs_size

def get_ebs_count(instance):
    ebs_volumes = instance.get('BlockDeviceMappings', [])
    ebs_count = len([volume for volume in ebs_volumes if 'Ebs' in volume])
    return ebs_count

def generate_server_details(instances, output_file):
    header = f"{'Instance ID':<20} {'Instance Type':<12} {'Status':<10} {'Private IP':<15} {'Public IP':<15} {'Total EBS Size (GB)':<20} {'EBS Count':<10} {'EBS Volume IDs':<40}\n"
    output_file.write(header.encode('utf-8'))
    output_file.write(b'-' * 145 + b'\n')  # Use b'-' to create a bytes literal
    total_ebs_size = 0
    for instance in instances:
        instance_id = instance['InstanceId']
        instance_type = instance['InstanceType']
        status = instance['State']['Name']
        private_ip = instance.get('PrivateIpAddress', 'N/A')
        public_ip = instance.get('PublicIpAddress', 'N/A')
        ebs_size = get_instance_ebs_size(instance)
        ebs_count = get_ebs_count(instance)
        ebs_volumes = instance.get('BlockDeviceMappings', [])
        ebs_volume_ids = ', '.join([volume['Ebs']['VolumeId'] for volume in ebs_volumes if 'Ebs' in volume])
        total_ebs_size += ebs_size

        instance_info = f"{instance_id:<20} {instance_type:<12} {status:<10} {private_ip:<15} {public_ip:<15} {ebs_size:>20} GB {ebs_count:<10} {ebs_volume_ids:<40}\n"
        output_file.write(instance_info.encode('utf-8'))

    total_ebs_size_info = f"\nTotal EBS Size of all servers: {total_ebs_size} GB\n"
    output_file.write(total_ebs_size_info.encode('utf-8'))


def lambda_handler(event, context):
    logger.addHandler(logging.StreamHandler(sys.stdout))

    ec2 = boto3.client('ec2', region_name='eu-central-1')
    
    search_input = "*" 
    
    filters = []
    if search_input != '*':
        filters.append({
            'Name': 'tag:Name',
            'Values': [search_input]
        })
    
    instances = ec2.describe_instances(Filters=filters)['Reservations']
    instances = [instance['Instances'][0] for instance in instances]
    
    instances.sort(key=lambda x: get_instance_ebs_size(x), reverse=True)

    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        tmp_file_path = tmp_file.name
        generate_server_details(instances, tmp_file)

    logger.info("Iniciando a função Lambda")
    
    search_input = "*"

    logger.info(f"Quantidade de instâncias: {len(instances)}")

    s3 = boto3.client('s3', region_name='eu-central-1')

    bucket_name = 'challenge-results'
    s3_key = f"relatorio_{context.aws_request_id}.txt"
    s3.upload_file(tmp_file_path, bucket_name, s3_key)

    os.remove(tmp_file_path)
