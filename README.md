# **Python3, Terraform and AWS**

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)
![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white)

## 💾 **ABOUT**
This repository was developed to solve a simple challenge 

**What were the requirements**

- Search a specific server by providing as input the name the EC2 server Name (tag) or search for all of them (by giving input '*' ). The search needs to be able to also filter on prefix (like search for all servers whose name starts with “server-*”.
- Given the search input, the script returns a table with a list of fields for each server:
instance-id, instance-type, status, private-ip, public-ip (if available), total-size-ebs-volumes (so the
sum of the sizes in GB of the attached volumes to the instance)
- The list of servers should be ordered by the total size of ebs volumes attached.
- At the end, print out the total size of all the ebs volumes in GB of all servers (as single number).
Please provide instructions to run the script.
- **Bonus points** 
If you have time, use terraform or terragrunt to deploy the script as lambda function. This is not
mandatory but is well considered in case of success.
Please test the deployment of terraform resources in your AWS account and then simply forward
to us the code that we will deploy in our Account.
Feel free to use terraform version > 1.0

## Getting started

I'll explain a little bit of how this project's folder structure works. As I had some time I created 2 different types of terraform with 2 different options.

```
    .
    ├── script_py_boto3            # Python3 script with Boto3 (To Run Individually)
    ├── script_py_boto3_s3         # Python3 script with Boto3 (To Run Individually) and store on s3
    ├── terraform_lambda           # Terraform script to create lambda function
    ├── images                     # Image directory for Readme
    └── README.md
```

## Using this repo

```
cd existing_repo
git remote add origin https://gitlab.com/mmodeneis/challenge_py_boto3.git
git branch -M main
git push -uf origin main
```

</div>

## Step by Step to use script_py_boto3 


- [Clone this repo](https://gitlab.com/mmodeneis/challenge_py_boto3.git)
- [Install and configure the AWS CLI in your environment](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- Configure your AWS CLI with the command aws configure
- Install Python and Boto3 (sudo apt-get install python-boto3)
- Navigate to the cloned repository folder
- And run ./script.py
  Command output result: 
  ![command output result](images/script_result.png)

We have the information: 
- InstanceID; 
- Instane Type;  
- Status; 
- Private IP;
- Public IP; 
- Total EBS Size (in GB); 
- EBS Count; 
- EBS Volume IDs; 
- Total EBS Size fo all servers;


## Step by Step to use terraform_lambda / terraform_lambda_s3

For part of Terraform I tested in my private AWS account, as the given keys don't have access to IAM and therefore it's not possible to create the lambda function! So I already took advantage and coded the lambda to save the results in an s3. That way we can also have a history of changes.

- [Clone this repo](https://gitlab.com/mmodeneis/challenge_py_boto3.git)
- [Install and configure terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- Validade your installation using terraform --version 

```
Terraform v1.5.6 on linux_amd64
+ provider registry.terraform.io/hashicorp/archive v2.4.0
+ provider registry.terraform.io/hashicorp/aws v5.14.0
```

- Assuming you already have the AWS cli installed and configured
- Navigate to the cloned repository folder
- And run terraform init
- And terraform plan
- And terraform apply

![command output terraform](images/terraform_result.png)

- To test lambda function use: 
```
aws lambda invoke --function-name EC2ServerInfo output.txt
```

![command lambda](images/lambda.png)
![log lambda](images/lambda_log.png)
![bucket](images/bucket.png)
## Authors
Author: Marcio Modeneis

Email: mmodeneis@gmail.com

